package com.rentit.inventory.application.services;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public List<PlantInventoryEntryDTO> findAvailable(String plantName, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> res = inventoryRepository.findAvailablePlants(plantName,startDate, endDate);
        return plantInventoryEntryAssembler.toResources(res);
    }

    public PlantReservation createPlantReservation(PlantInventoryEntryDTO plant, BusinessPeriod reservationTime) {
        PlantReservation plantReservation = new PlantReservation();
        PlantInventoryItem plantItem = plantInventoryItemRepository.findOneByIdEquals(plant.get_id());
        plantReservation.setPlant(plantItem);
        plantReservation.setSchedule(reservationTime);
        plantReservation = plantReservationRepository.save(plantReservation);

        return plantReservation;
    }

}
