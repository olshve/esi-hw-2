package com.rentit.sales.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.services.InventoryService;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.exceptions.PONotFoundException;
import com.rentit.sales.application.services.PurchaseOrderAssembler;
import com.rentit.sales.application.services.SalesService;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/sales")
public class SalesRestController {

    @Autowired
    InventoryService inventoryService;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    SalesService salesService;
    @Autowired
    ObjectMapper mapper;

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailable(plantName, startDate, endDate);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPurchaseOrder(id);
    }

    @PostMapping("/orders")
    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws URISyntaxException, PlantNotFoundException {
        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(partialPODTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(newlyCreatePODTO.getId().getHref()));

        return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
        // Code To handle Exception
    }

    @GetMapping("/orders/pending")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> findPendingPOs() {
        return salesService.findPendingPurchaseOrders();
    }

    @PutMapping("/orders/preallocate")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PurchaseOrderDTO> preallocatePurchaseOrder(@RequestBody Long POId) throws URISyntaxException, PONotFoundException, PlantNotFoundException {
        PurchaseOrder po = purchaseOrderRepository.findOne(POId);
        List<PlantInventoryEntryDTO> plants = inventoryService.findAvailable(
                po.getPlant().getName(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate()
        );
        HttpHeaders headers = new HttpHeaders();
        PurchaseOrder responsePO;
        HttpStatus responseStatus = HttpStatus.CREATED;
        if (plants.size() > 0) {
            PlantInventoryEntryDTO plant = plants.get(1);
            PlantReservation plantReservation = inventoryService.createPlantReservation(plant, po.getRentalPeriod());
            // because we it is defined with getters only. for purpose I guess :(
            purchaseOrderRepository.delete(po);
            responsePO = PurchaseOrder.of(po.getPlant(), po.getRentalPeriod(), POStatus.PREALLOCATED);
            purchaseOrderRepository.save(responsePO);
        } else {
            // because we it is defined with getters only. for purpose I guess :(
            purchaseOrderRepository.delete(po);
            responsePO = PurchaseOrder.of(po.getPlant(), po.getRentalPeriod(), POStatus.REJECTED);
            purchaseOrderRepository.save(responsePO);
            responseStatus = HttpStatus.CONFLICT;
        }
        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderAssembler.toResource(responsePO), headers, responseStatus);
    }
}
